ARG ELIXIR_VERSION

FROM elixir:${ELIXIR_VERSION}

RUN dialyzer --build_plt --apps erts kernel stdlib crypto
